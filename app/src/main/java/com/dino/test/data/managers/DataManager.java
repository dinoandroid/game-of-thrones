package com.dino.test.data.managers;

import android.widget.Toast;

import com.activeandroid.query.Select;
import com.dino.test.data.model.entity.CharactersEntity;
import com.dino.test.data.model.entity.HousesEntity;
import com.dino.test.data.model.response.CharactersRes;
import com.dino.test.data.model.response.HousesRes;
import com.dino.test.data.network.TestService;
import com.dino.test.ui.fragment.StartLoadingFragment;
import com.dino.test.utils.Configuration;
import com.dino.test.utils.NetworkStatusChecker;
import com.dino.test.utils.TestApp;

import java.util.List;

import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by user on 12.10.2016.
 */
public class DataManager {

    protected TestService mVitaService;
    StartLoadingFragment fragment;

    public DataManager() {
        mVitaService = TestService.Factory.makeVitaService();
    }

    public void loadData(final StartLoadingFragment fragment) {
        this.fragment = fragment;
        if(NetworkStatusChecker.isNetworkAvailable())
        if (new Select().from(CharactersEntity.class).execute().size() == 0) {
            loadStark();
        } else {
            fragment.stopLoad();
        }
        else {
            Toast.makeText(TestApp.getContext(),"Нет подключения к интернету",Toast.LENGTH_LONG).show();
            fragment.stopLoad();
        }
    }

    private void loadStark() {
        mVitaService.getHouses(Configuration.STARK_ID_HOUSES).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Response<HousesRes>>() {
            @Override
            public void onCompleted() {
                loadLanister();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Response<HousesRes> housesResResponse) {
                HousesRes data = housesResResponse.body();
                new HousesEntity(data);
                loadCharacters(data.swornMembers, Configuration.STARK_ID_HOUSES);
            }
        });
    }

    private void loadLanister() {
        mVitaService.getHouses(Configuration.LANNISTER_ID_HOUSES).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Response<HousesRes>>() {
            @Override
            public void onCompleted() {
                loadTargarien();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Response<HousesRes> housesResResponse) {
                HousesRes data = housesResResponse.body();
                new HousesEntity(data);
                loadCharacters(data.swornMembers, Configuration.LANNISTER_ID_HOUSES);
            }
        });
    }

    private void loadTargarien() {
        mVitaService.getHouses(Configuration.TARGARYEN_ID_HOUSES).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Response<HousesRes>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Response<HousesRes> housesResResponse) {
                HousesRes data = housesResResponse.body();
                new HousesEntity(data);
                loadCharacters(data.swornMembers,Configuration.TARGARYEN_ID_HOUSES);
            }
        });
    }

    private void loadCharacters(final List<String> characters, final int id) {
        for (final String character : characters) {
            mVitaService.getCharacter(Integer.parseInt(character.split("/")[character.split("/").length - 1])).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Response<CharactersRes>>() {
                @Override
                public void onCompleted() {
                    if (id == Configuration.TARGARYEN_ID_HOUSES) {
                        if (character.equals(characters.get(characters.size() - 1))) {
                            fragment.stopLoad();
                        }
                    }
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(Response<CharactersRes> charactersResResponse) {
                    new CharactersEntity(charactersResResponse.body());
                }
            });
        }
    }
}
