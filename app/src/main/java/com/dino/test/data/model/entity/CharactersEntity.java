package com.dino.test.data.model.entity;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.dino.test.data.model.response.CharactersRes;
import com.dino.test.utils.UtilListSerializer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 12.10.2016.
 */
@Table(name = "CharactersEntity")
public class CharactersEntity extends Model {
    @Column(name = "url")
    public String url;
    @Column(name = "name")
    public String name;
    @Column(name = "gender")
    public String gender;
    @Column(name = "culture")
    public String culture;
    @Column(name = "born")
    public String born;
    @Column(name = "died")
    public String died;
    @Column(name = "titles")
    public String titles;
    @Column(name = "aliases")
    public String aliases;
    @Column(name = "father")
    public String father;
    @Column(name = "mother")
    public String mother;
    @Column(name = "spouse")
    public String spouse;
    @Column(name = "allegiances")
    public String allegiances;
    @Column(name = "books")
    public List<String> books = new ArrayList<String>();
    @Column(name = "povBooks")
    public List<Object> povBooks = new ArrayList<Object>();
    @Column(name = "tvSeries")
    public List<Object> tvSeries = new ArrayList<Object>();
    @Column(name = "playedBy")
    public List<Object> playedBy = new ArrayList<Object>();

    public CharactersEntity() {

    }

    public List<String> getTitle() {
        return new UtilListSerializer().deserialize(titles);
    }

    public List<String> getAliases() {
        return new UtilListSerializer().deserialize(aliases);
    }

    public CharactersEntity(CharactersRes res) {
        this.url = res.url;
        this.name = res.name;
        this.gender = res.gender;
        this.culture = res.culture;
        this.born = res.born;
        this.died = res.died;
        this.titles = new UtilListSerializer().serialize(res.getTitle());
        if (res.getTitle() != null)
            Log.e("debug_title", "" + res.getTitle().size() + " " + res.name);
        this.aliases = new UtilListSerializer().serialize(res.getAliase());
        this.father = res.father;
        Log.e("debug_father", "" + res.father + " " + res.name);
        this.mother = res.mother;
        this.spouse = res.spouse;
        this.allegiances = res.allegiances.get(0);
        this.books = res.books;
        this.povBooks = res.povBooks;
        this.tvSeries = res.tvSeries;
        this.playedBy = res.playedBy;
        save();
    }
}
