package com.dino.test.data.model.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.dino.test.data.model.response.HousesRes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 12.10.2016.
 */
@Table(name = "HousesEntity")
public class HousesEntity extends Model {
    @Column(name = "url")
    public String url;
    @Column(name = "name")
    public String name;
    @Column(name = "region")
    public String region;
    @Column(name = "coatOfArms")
    public String coatOfArms;
    @Column(name = "words")
    public String words;
    @Column(name = "titles")
    public List<String> titles = new ArrayList<String>();
    @Column(name = "seats")
    public List<String> seats = new ArrayList<String>();
    @Column(name = "currentLord")
    public String currentLord;
    @Column(name = "heir")
    public String heir;
    @Column(name = "overlord")
    public String overlord;
    @Column(name = "founded")
    public String founded;
    @Column(name = "founder")
    public String founder;
    @Column(name = "diedOut")
    public String diedOut;
    @Column(name = "ancestralWeapons")
    public List<String> ancestralWeapons = new ArrayList<String>();
    @Column(name = "cadetBranches")
    public List<String> cadetBranches = new ArrayList<String>();
    @Column(name = "swornMembers")
    public List<String> swornMembers = new ArrayList<String>();

    public HousesEntity() {

    }

    public HousesEntity(HousesRes res) {
        this.url = res.url;
        this.name = res.name;
        this.region = res.region;
        this.coatOfArms = res.coatOfArms;
        this.words = res.words;
        this.seats = res.seats;
        this.titles = res.titles;
        this.currentLord = res.currentLord;
        this.heir = res.heir;
        this.overlord = res.overlord;
        this.founded = res.founded;
        this.founder = res.founder;
        this.diedOut = res.diedOut;
        this.ancestralWeapons = res.ancestralWeapons;
        this.cadetBranches = res.cadetBranches;
        this.swornMembers = res.swornMembers;
        save();
    }
}
