package com.dino.test.data.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 12.10.2016.
 */
public class CharactersRes {

    public String url;

    public String name;

    public String gender;

    public String culture;

    public String born;

    public String died;

    private List<String> titles = new ArrayList<String>();

    private List<String> aliases = new ArrayList<String>();

    public String father;

    public String mother;

    public String spouse;

    public List<String> allegiances = new ArrayList<String>();

    public List<String> books = new ArrayList<String>();

    public List<Object> povBooks = new ArrayList<Object>();

    public List<Object> tvSeries = new ArrayList<Object>();

    public List<Object> playedBy = new ArrayList<Object>();

    public List<String> getTitle() {
        return titles;
    }

    public List<String> getAliase() {
        return aliases;
    }

}
