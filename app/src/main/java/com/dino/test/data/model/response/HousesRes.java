package com.dino.test.data.model.response;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 12.10.2016.
 */
public class HousesRes {

    public String url;

    public String name;

    public String region;

    public String coatOfArms;

    public String words;

    public List<String> titles = new ArrayList<String>();

    public List<String> seats = new ArrayList<String>();

    public String currentLord;

    public String heir;

    public String overlord;

    public String founded;

    public String founder;

    public String diedOut;

    public List<String> ancestralWeapons = new ArrayList<String>();

    public List<String> cadetBranches = new ArrayList<String>();

    public List<String> swornMembers = new ArrayList<String>();

}