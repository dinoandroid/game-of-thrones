package com.dino.test.data.network;


import com.dino.test.BuildConfig;
import com.dino.test.data.model.response.CharactersRes;
import com.dino.test.data.model.response.HousesRes;
import com.dino.test.utils.Configuration;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.squareup.moshi.Moshi;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by user on 12.10.2016.
 */
public interface TestService {

    @GET("/api/houses/{num}")
    Observable<Response<HousesRes>> getHouses(@Path("num") int num);

    @GET("/api/characters/{num}")
    Observable<Response<CharactersRes>> getCharacter(@Path("num") int num);

    class Factory {

        public static TestService makeVitaService() {

            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
            CookieHandler.setDefault(cookieManager);

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BASIC : HttpLoggingInterceptor.Level.NONE);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(Configuration.RESPONSE_WAITING_TIME, TimeUnit.SECONDS)
                    .readTimeout(Configuration.RESPONSE_WAITING_TIME, TimeUnit.SECONDS)
                    .addNetworkInterceptor(new StethoInterceptor())
                    .addInterceptor(logging)
                    .addInterceptor(new HttpLoggingInterceptor())
                    .cookieJar(new JavaNetCookieJar(cookieManager))
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Configuration.TEST_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();

            return retrofit.create(TestService.class);
        }

    }
}
