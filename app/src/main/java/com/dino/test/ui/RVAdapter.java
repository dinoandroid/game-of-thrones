package com.dino.test.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dino.test.R;
import com.dino.test.data.model.entity.CharactersEntity;
import com.dino.test.utils.Configuration;
import com.dino.test.utils.TestApp;

import java.util.List;

/**
 * Created by user on 12.10.2016.
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder> {
    private List<CharactersEntity> data;
    private IClickCharacter interfaces;
    private int id;

    public RVAdapter(List<CharactersEntity> data, IClickCharacter interfaces, int id) {
        this.data = data;
        this.id = id;
        this.interfaces = interfaces;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        PersonViewHolder viewHolder = new PersonViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, final int position) {
        holder.personName.setText(data.get(position).name);
        if (data.get(position).getAliases().size() > 0 && !data.get(position).getAliases().get(0).equals(""))
            holder.slogan.setText(data.get(position).getAliases().get(0));
        else if (data.get(position).getTitle().size() > 0 && !data.get(position).getTitle().get(0).equals(""))
            holder.slogan.setText(data.get(position).getTitle().get(0));
        else holder.slogan.setVisibility(View.GONE);

        switch (id) {
            case Configuration.STARK_ID_HOUSES:
                holder.logo.setImageDrawable(TestApp.getContext().getResources().getDrawable(R.drawable.stark_icon));
                break;
            case Configuration.LANNISTER_ID_HOUSES:
                holder.logo.setImageDrawable(TestApp.getContext().getResources().getDrawable(R.drawable.lanister_icon));
                break;
            case Configuration.TARGARYEN_ID_HOUSES:
                holder.logo.setImageDrawable(TestApp.getContext().getResources().getDrawable(R.drawable.targarien_icon));
                break;
        }
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interfaces.ClickCharacter(data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv;
        TextView personName;
        TextView slogan;
        ImageView logo;

        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (LinearLayout) itemView.findViewById(R.id.cv);
            logo = (ImageView) itemView.findViewById(R.id.logo);
            personName = (TextView) itemView.findViewById(R.id.person_name);
            slogan = (TextView) itemView.findViewById(R.id.slogan);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface IClickCharacter {
        void ClickCharacter(CharactersEntity mEntity);
    }
}