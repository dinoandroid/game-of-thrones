package com.dino.test.ui;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dino.test.data.managers.DataManager;
import com.dino.test.ui.fragment.PageFragment;
import com.dino.test.utils.Configuration;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<PageFragment> mFragment = new ArrayList<>();
    private String tabTitles[] = new String[]{"STARKS", "LANNISTERS", "TARGARYENS"};

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragment.add(PageFragment.newInstance(Configuration.STARK_ID_HOUSES));
        mFragment.add(PageFragment.newInstance(Configuration.LANNISTER_ID_HOUSES));
        mFragment.add(PageFragment.newInstance(Configuration.TARGARYEN_ID_HOUSES));
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mFragment.size();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return mFragment.get(0);
            case 1:
                return mFragment.get(1);
            case 2:
                return mFragment.get(2);
            default:
                return mFragment.get(0);
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

}
