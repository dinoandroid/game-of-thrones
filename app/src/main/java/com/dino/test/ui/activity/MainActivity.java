package com.dino.test.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.dino.test.R;
import com.dino.test.ui.fragment.MainFragment;
import com.dino.test.ui.fragment.StartLoadingFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new StartLoadingFragment()).commit();
        }
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager()!=null && getSupportFragmentManager().getBackStackEntryCount()!=0)
        if (getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName().equals("CharacterFragment")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new MainFragment()).commit();
        } else super.onBackPressed();
    }
}
