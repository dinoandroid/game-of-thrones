package com.dino.test.ui.fragment;


import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.dino.test.R;
import com.dino.test.data.managers.DataManager;
import com.dino.test.data.model.entity.CharactersEntity;
import com.dino.test.data.model.entity.HousesEntity;
import com.dino.test.ui.activity.MainActivity;
import com.dino.test.utils.Configuration;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharacterFragment extends Fragment {

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.imageHouses)
    ImageView imageHouses;
    @BindView(R.id.text_words)
    TextView mTextWords;
    @BindView(R.id.scroll)
    NestedScrollView scroll;
    @BindView(R.id.layout_words)
    LinearLayout mLayoutWords;
    @BindView(R.id.text_born)
    TextView mTextBorn;
    @BindView(R.id.layout_born)
    LinearLayout mLayoutBorn;
    @BindView(R.id.text_titles)
    TextView mTextTitles;
    @BindView(R.id.layout_titles)
    LinearLayout mLayoutTitles;
    @BindView(R.id.text_aliases)
    TextView mTextAliases;
    @BindView(R.id.layout_aliases)
    LinearLayout mLayoutAliases;
    @BindView(R.id.btn_father)
    Button mBtnFather;
    @BindView(R.id.layout_father)
    LinearLayout mLayoutFather;
    @BindView(R.id.btn_mother)
    Button mBtnMother;
    @BindView(R.id.layout_mother)
    LinearLayout mLayoutMother;
    CharactersEntity mCharacterEntity;
    HousesEntity mHouses;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public static CharacterFragment newInstance(String url, int id) {
        Bundle args = new Bundle();
        args.putString("url", url);
        args.putInt("id", id);
        CharacterFragment fragment = new CharacterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_character, container, false);
        ButterKnife.bind(this, mRootView);
        initToolbar();
        initData();
        initName();
        initWords();
        initBorn();
        initTitles();
        initAliases();
        initFather();
        initMother();
        initDead();
        setHasOptionsMenu(true);
        return mRootView;
    }

    private void initData() {
        mCharacterEntity = new Select().from(CharactersEntity.class).where("url = ?", getArguments().getString("url")).executeSingle();
        mHouses = new Select().from(HousesEntity.class).where("url = ?", "https://anapioficeandfire.com/api/houses/" + getArguments().getInt("id")).executeSingle();
    }

    private void initToolbar() {
        ((MainActivity) getActivity()).setSupportActionBar(mToolbar);
        if (((MainActivity) getActivity()).getSupportActionBar() != null) {
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void initDead() {
        if (!mCharacterEntity.died.equals("") && mCharacterEntity.died != null) {
            Snackbar snackbar = Snackbar
                    .make(scroll, "Умер", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void initName() {
        if (mCharacterEntity.name != null && !mCharacterEntity.name.equals(""))
            collapsingToolbarLayout.setTitle(mCharacterEntity.name);

        switch (getArguments().getInt("id")) {
            case Configuration.STARK_ID_HOUSES:
                imageHouses.setImageResource(R.drawable.stark);
                break;
            case Configuration.LANNISTER_ID_HOUSES:
                imageHouses.setImageResource(R.drawable.lannister);
                break;
            case Configuration.TARGARYEN_ID_HOUSES:
                imageHouses.setImageResource(R.drawable.targarien);
                break;
        }
    }

    private void initMother() {
        CharactersEntity mother = new Select().from(CharactersEntity.class).where("url = ?", mCharacterEntity.mother).executeSingle();
        if (mother != null) {
            mBtnMother.setText(mother.name);
            mBtnMother.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, CharacterFragment.newInstance(mCharacterEntity.mother, getArguments().getInt("id"))).addToBackStack("").commit();
                }
            });
        } else {
            mLayoutMother.setVisibility(View.GONE);
        }
    }

    private void initFather() {
        CharactersEntity father = new Select().from(CharactersEntity.class).where("url = ?", mCharacterEntity.father).executeSingle();
        if (father != null) {
            mBtnFather.setText(father.name);
            mBtnFather.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, CharacterFragment.newInstance(mCharacterEntity.father, getArguments().getInt("id"))).addToBackStack("").commit();
                }
            });
        } else {
            mLayoutFather.setVisibility(View.GONE);
        }
    }

    private void initAliases() {
        String aliases = "";
        if (mCharacterEntity.getAliases().size() != 0 && !mCharacterEntity.getAliases().get(0).equals("")) {
            for (Object aliase : mCharacterEntity.getAliases()) {
                aliases = aliases + ((String) aliase) + "\n";
            }
            mTextAliases.setText(aliases);
        } else {
            mLayoutAliases.setVisibility(View.GONE);
        }
    }

    private void initTitles() {
        String titles = "";
        if (mCharacterEntity.getTitle().size() != 0 && !mCharacterEntity.getTitle().get(0).equals("")) {
            for (Object title : mCharacterEntity.getTitle()) {
                titles = titles + ((String) title) + "\n";
            }
            mTextTitles.setText(titles);

        } else {
            mLayoutAliases.setVisibility(View.GONE);
        }
    }

    private void initBorn() {
        if (mCharacterEntity.born != null && !mCharacterEntity.born.equals(""))
            mTextBorn.setText(mCharacterEntity.born);
        else {
            mLayoutBorn.setVisibility(View.GONE);
        }
    }

    private void initWords() {
        if (mHouses.words != null && !mHouses.words.equals(""))
            mTextWords.setText(mHouses.words);
        else {
            mLayoutWords.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


}
