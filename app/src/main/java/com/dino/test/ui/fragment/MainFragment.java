package com.dino.test.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dino.test.R;
import com.dino.test.ui.ViewPagerAdapter;
import com.dino.test.ui.activity.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 12.10.2016.
 */
public class MainFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    protected DrawerLayout mDrawer;
    @BindView(R.id.navigation_view)
    protected NavigationView mNavigationView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.sliding_tabs)
    TabLayout tabLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, mRootView);
        initView();
        return mRootView;
    }

    private void initView() {
        ((MainActivity) getActivity()).setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();
        mDrawer.setDrawerListener(toggle);
        mNavigationView.setNavigationItemSelectedListener(this);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_starks:
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                mDrawer.closeDrawer(Gravity.LEFT);
                                tabLayout.getTabAt(0).select();
                            }
                        }, 100);
                break;
            case R.id.item_lannisters:
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                mDrawer.closeDrawer(Gravity.LEFT);
                                tabLayout.getTabAt(1).select();
                            }
                        }, 100);
                break;
            case R.id.item_targaryens:
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                mDrawer.closeDrawer(Gravity.LEFT);
                                tabLayout.getTabAt(2).select();
                            }
                        }, 100);
                break;
        }

        return false;
    }

}

