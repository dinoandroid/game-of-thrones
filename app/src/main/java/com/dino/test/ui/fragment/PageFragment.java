package com.dino.test.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.activeandroid.query.Select;
import com.dino.test.R;
import com.dino.test.data.model.entity.CharactersEntity;
import com.dino.test.ui.RVAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by SoftDesign03 on 12.10.2016.
 */

public class PageFragment extends Fragment implements RVAdapter.IClickCharacter {

    public static final String ARG_ID = "ARG_ID";
    private List<CharactersEntity> mCharacters;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    private RVAdapter adapter;
    private int id;

    public static PageFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_ID, page);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void initRecyclerView() {
        if (id == 0)
            id = getArguments().getInt(ARG_ID);
        mCharacters = new Select().from(CharactersEntity.class).where("allegiances = ?", "https://anapioficeandfire.com/api/houses/" + id).execute();
        adapter = new RVAdapter(mCharacters, this, id);
        mRecyclerView.setAdapter(adapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            id = savedInstanceState.getInt("id");
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler, container, false);
        ButterKnife.bind(this, view);
        initRecyclerView();
        return view;
    }


    @Override
    public void ClickCharacter(CharactersEntity mEntity) {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, CharacterFragment.newInstance(mEntity.url, getArguments().getInt(ARG_ID))).addToBackStack("CharacterFragment").commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("id", id);
        super.onSaveInstanceState(outState);
    }
}
