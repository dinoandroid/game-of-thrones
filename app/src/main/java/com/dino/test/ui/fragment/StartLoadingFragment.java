package com.dino.test.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.activeandroid.query.Select;
import com.dino.test.R;
import com.dino.test.data.managers.DataManager;
import com.dino.test.data.model.entity.CharactersEntity;


public class StartLoadingFragment extends Fragment {

    private DataManager mDataManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDataManager = new DataManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_start_loading, container, false);
        mDataManager.loadData(this);
        return mRootView;
    }

    public void stopLoad() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getFragmentManager().beginTransaction().replace(R.id.container, new MainFragment()).commit();
            }
        }, 3000);
    }

}
