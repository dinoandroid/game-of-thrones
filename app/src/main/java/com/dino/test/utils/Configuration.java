package com.dino.test.utils;

/**
 * Created by user on 12.10.2016.
 */
public class Configuration {
    public static String TEST_URL = "https://anapioficeandfire.com/";
    public static final int RESPONSE_WAITING_TIME = 40;
    public static final int STARK_ID_HOUSES = 362;
    public static final int LANNISTER_ID_HOUSES = 229;
    public static final int TARGARYEN_ID_HOUSES = 378;
}
