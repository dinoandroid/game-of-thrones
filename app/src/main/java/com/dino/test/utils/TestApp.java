package com.dino.test.utils;

import android.app.Application;
import android.content.Context;

import com.activeandroid.ActiveAndroid;

/**
 * Created by user on 12.10.2016.
 */
public class TestApp extends Application {
    public static TestApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        instance = this;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }
}